from django.db import models

# Create your models here.
class Student(models.Model):
    sname=models.CharField(max_length=50)
    sage=models.IntegerField()
    saddr=models.CharField(max_length=60)
    profile_pic=models.ImageField(null=True, blank=True, upload_to="images/")
    def __str__(self):
        return self.sname
